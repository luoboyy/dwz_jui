/**
 * 添加DWZ对于require的支持，为了规范JS引入、减少内存溢出和网络请求。
 * 要求片段页面中不能再包含script标签，只能使用本插件规定的require标签来引入require模块进行页面初始化和销毁。
 * require模块需要提供init($parent, obj)方法和destroy($parent, obj)方法来进行初始化和销毁。其中destroy不是必须的。
 * @author ZYB
 * @param DWZ
 * @param require
 * @returns
 */
(function(DWZ, require){
	
	function pluginRequire($p){
		$p.find('require').each(function(){
			var $me = $(this), param = $me.attr('param'), moduleName = $me.attr('name');
			if(!moduleName){
				alertMsg.warn('当前页面包含不合法的require标签（{tag}）'.replaceTm({tag:$me[0].outerHTML}).encodeTXT());
			}else if(/\.\bjs\b/.test(moduleName)){
				alertMsg.warn('当前页面包含不合法的require标签（{tag}），原则上不允许引入JS文件，需要指定require模块名称。'.replaceTm({tag:$me[0].outerHTML}).encodeTXT());
				try{
					require([moduleName]);
				}catch(e){}
			}else{
				require([moduleName], function(module){
					if(module && module.init){
						try{
							module.init($p, param);
						}catch(e){console && console.error(e);}
						$p.bind(DWZ.eventType.pageClear, function(event){
							if($.isFunction(module.destroy)){
								module.destroy(event, $p, param);
							}
							DWZ.debug('Destroyed ' + $p);
						});
					}else{
						alertMsg.error('当前页面包含不合法的require标签（{tag}），标签引入的模块没有init方法。'.replaceTm({tag:$me[0].outerHTML}).encodeTXT());
					}
				});
			}
			try{
				param = DWZ.jsonEval(param );
			}catch(e){param = {};}
			
		});
	}
	DWZ.regPlugins.push(pluginRequire);
})(DWZ, require);